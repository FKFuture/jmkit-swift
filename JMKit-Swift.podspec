#
#  Be sure to run `pod spec lint JMKit.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|


  s.name         = "JMKit-Swift"
    s.version      = "0.6.2"
    s.summary      = "UI Fast Use for iOS"
    s.description  = "快速创建文本，按钮，输入框，设置各种控件属性，快速完成UI"

    s.homepage     = "https://gitlab.com/FKFuture/jmkit-swift.git"
    s.license              =  "MIT"

    s.author             = { "JM" => "asdfgwjm@163.com" }
    s.source       = { :git => "https://gitlab.com/FKFuture/jmkit-swift.git", :tag => "0.6.2" }

  s.subspec 'JMExtension' do |mt|
    mt.source_files = 'Demo/SwiftJMKit/JMKit/JMExtension/*.{swift}'
    mt.dependency 'SnapKit'

  end

  s.subspec 'JMFactory' do |mf|
    mf.source_files = 'Demo/SwiftJMKit/JMKit/JMFactory/*.{swift}'
    mf.dependency 'MJRefresh'
    mf.dependency 'SnapKit'
    mf.dependency 'JMKit-Swift/JMExtension'


  end
  s.subspec 'Tool' do |ms|
    ms.source_files = 'Demo/SwiftJMKit/JMKit/Tool/*.{swift}'
    ms.dependency 'JMKit-Swift/JMFactory'
    ms.dependency 'SnapKit'


  end

    s.platform     = :ios, "10.0"
    s.swift_versions = "5.0"
    s.frameworks = "Foundation","UIKit"


  # s.public_header_files = "Classes/**/*.h"


  # s.resource  = "icon.png"
  # s.resources = "Resources/*.png"

  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"



  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"

end
