//
//  JMNavigationExtension.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/22.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

public extension UINavigationController{
    
    /// 设置导航栏背景颜色，但是颜色值会有一些区别，要比原先颜色浅一些
    /// - Parameter color: 颜色
    func setNavigationBackGroundColor(color:UIColor)  {
        
        self.navigationBar.barTintColor = color
    }
    
    /// 去掉导航背景设置颜色
    /// - Parameter color: 颜色
    func dismissBackGroundWithColor(color:UIColor)  {
        
        self.navigationBar.setBackgroundImage(UIImage.init(), for: .default)
        self.navigationBar.shadowImage = UIImage.init()
        self.navigationBar.backgroundColor = color
    }
    
    /// 设置导航背景颜色为系统默认颜色
    func recoverBackGroundColor() {
        
        let bar = UINavigationBar.init()
        self.navigationBar.setBackgroundImage(bar.shadowImage, for: .default)
        
    }
    
    /// 设置导航栏标题
    /// - Parameters:
    ///   - color: 颜色
    ///   - font: 字体大小
    func setNavigationBarTitleColor(color:UIColor?,font:UIFont?)  {
        
        let dic = NSMutableDictionary.init()
        
        if (color != nil) {
            dic.setValue(color, forKey:NSAttributedString.Key.foregroundColor.rawValue)
        }
        
        if font != nil {
            dic.setValue(font, forKey: NSAttributedString.Key.font.rawValue)
        }
        
        self.navigationBar.titleTextAttributes = dic.copy() as? [NSAttributedString.Key : Any]
        
        
    }
    
    //设置UIBarButtonItem
    func setNavigationBarItemColor(color:UIColor)  {
        
        self.navigationBar.tintColor = color
        
    }

    
    /// 设置导航只返回一个箭头
    func dismissNavigationBackDirection() {
        
        UIBarButtonItem .appearance().setBackButtonTitlePositionAdjustment(UIOffset.init(horizontal: 0, vertical: -60), for: .default)
        
    }
    
    func setRightItemButton(button:UIButton,rightMargin:CGFloat,imageSize:CGSize)  {
        
        let item = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = item
        
        
    }

    
    

    
}



