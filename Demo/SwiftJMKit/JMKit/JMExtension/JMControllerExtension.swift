//
//  JMControllerExtension.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/22.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

public extension UIViewController{

    
    // MARK: 系统弹窗相关
    
    /// 系统标准弹窗
    /// - Parameters:
    ///   - title: 标题
    ///   - subTitle: 副标题
    ///   - mode: 弹窗模式
    ///   - cancleBlock: 取消回调
    ///   - sureBlock: 确定回调
    ///   - completeBlock: 弹窗显示后回调
    func showAlert(title:String,subTitle:String,mode:UIAlertController.Style,cancleBlock:@escaping () -> Void,sureBlock: @escaping () -> Void,completeBlock:@escaping () -> Void)  {

        let alertController = UIAlertController(title: title, message: subTitle, preferredStyle: mode)

        let alertAction = UIAlertAction(title: "确定", style: .default) { (_) in

            sureBlock()
        }
        
        let cancleAction = UIAlertAction(title: "取消", style: .cancel) { (_) in

            cancleBlock()
        }


        alertController.addAction(alertAction)
        alertController.addAction(cancleAction)

        present(alertController, animated: true) {
            
            completeBlock()
        };

    }
    
    /// 系统标准弹窗
    /// - Parameters:
    ///   - title: 标题
    ///   - subTitle: 副标题
    ///   - mode: 弹窗模式
    ///   - actionList: action列表
    ///   - completeBlock: 弹窗显示后回调
    func showAlert(title:String,subTitle:String,mode:UIAlertController.Style, actionList:Array<UIAlertAction>,completeBlock:@escaping () -> Void)  {
        
        let alertController = UIAlertController(title: title, message: subTitle, preferredStyle: mode)
        
        for i in 0..<actionList.count {
            
            let action:UIAlertAction = actionList[i]
            
            alertController.addAction(action)
            
        }
        
        present(alertController, animated: true) {
            
            completeBlock()
        };

    }
    
    // MARK: 导航返回相关

    func backIgoredController(igoredClass:AnyClass) {
        
        let viewControllers:[UIViewController] = self.navigationController?.viewControllers ?? []
        
        let allCount:NSInteger = viewControllers.count
        
        let targetViewControllers = NSMutableArray.init()
        
        for i in 0..<allCount {
            
            let vc:UIViewController = viewControllers[i]
            
            if vc.isKind(of: igoredClass) {
                break
            }else if (vc == self){
                break
            }
            
            targetViewControllers.add(vc)
            
        }
        
        targetViewControllers .add(self)
        self.navigationController?.viewControllers = targetViewControllers as! [UIViewController]
        self.navigationController?.popViewController(animated: true)
        
        
    }
    
    /// 返回到指定的控制器，若未找到则返回上一级
    /// - Parameter limitClass: 指定控制器类
    func backToLimitController(limitClass:AnyClass) {
        
        let viewControllers:[UIViewController] = self.navigationController?.viewControllers ?? []

        let allCount:NSInteger = viewControllers.count

        
        guard allCount > 1 else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        for i in 0..<allCount {
            
            let vc:UIViewController = viewControllers[i]
            
            if vc.isKind(of: limitClass) {
                self.navigationController?.popToViewController(vc, animated: true)
                break
            }
            
        }
        
        self.navigationController?.popViewController(animated: true)
        
        
    }



}

