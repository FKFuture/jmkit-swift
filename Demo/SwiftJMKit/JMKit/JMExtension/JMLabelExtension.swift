//
//  JMLabelExtension.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/23.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

public extension UILabel{
    
    /// 获取文本中的指定字符所有范围数组
    /// - Parameter regex: 需要查找的字符
    /// - Returns: 字符范围数组
    private func getKeyRange(regex:String) -> [NSRange]{
        do {
            let regex: NSRegularExpression = try NSRegularExpression(pattern: regex, options: [])
            let matches = regex.matches(in: self.text ?? "", options: [], range: NSMakeRange(0, self.text?.count ?? 0))
            
            var data:[NSRange] = Array()
            for item in matches {
                data.append(item.range)
            }
            
            return data
        }
        catch {
            return []
        }
    }

    ///修改文本中指定字符串的字体大小/颜色（若文本中存在多个此文本会全部修改）
    /// - Parameters:
    ///   - text: 指定文本
    ///   - font: 字体大小
    ///   - color: 字体颜色
    func changeRepeatCharacter(RepeatKey:String,font:UIFont?,color:UIColor?)
    {
        
        guard RepeatKey.count > 0 else {
            return
        }
        
        let resultArray = self.getKeyRange(regex: RepeatKey)
        
        for i in 0..<resultArray.count {
            
            let range:NSRange = resultArray[i]
            self.changeCharacter(range: range, font: font, color: color)
        }
        
    }

    /// 修改文本中指定字符串的字体大小/颜色（若文本中存在多个此文本只修改第一个）
    /// - Parameters:
    ///   - text: 指定文本
    ///   - font: 字体大小
    ///   - color: 字体颜色
    func changeCharacter(key:String, font:UIFont?, color:UIColor?)  {

        let labelString = self.text! as NSString

        let rang = labelString.range(of: key)
        
        guard rang.location != NSNotFound else {
            return
        }

        let startIndex = rang.location + rang.length

        let attributed:NSMutableAttributedString = self.attributedText?.mutableCopy() as! NSMutableAttributedString

        attributed.addAttributes([NSAttributedString.Key.foregroundColor:color ?? self.textColor as UIColor,NSAttributedString.Key.font:font ?? self.font as UIFont], range: NSRange(location: startIndex, length: labelString.length - startIndex))

        self.attributedText = attributed

    }


    
    /// 修改文本中指定数组中的字体大小颜色
    /// - Parameters:
    ///   - array: 文本数组
    ///   - font: 字体大小
    ///   - color: 字体颜色
    func changeCharacter(array:NSArray ,font:UIFont,color:UIColor)  {

        let attributed:NSMutableAttributedString = self.attributedText?.mutableCopy() as! NSMutableAttributedString

        let labelString = self.text! as NSString


        for text in array {

            let rang = labelString.range(of: text as! String)

            attributed.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: rang)
            attributed.addAttribute(NSAttributedString.Key.font, value: font, range: rang)


        }

        self.attributedText = attributed


    }
    
    /// 修改文本中指定数组中的字体大小颜色（若文本中存在多个此文本会全部修改）
    /// - Parameters:
    ///   - array: 文本数组
    ///   - font: 字体大小
    ///   - color: 字体颜色
    func changeRepeatCharacter(array:NSArray ,font:UIFont,color:UIColor)  {

        let attributed:NSMutableAttributedString = self.attributedText?.mutableCopy() as! NSMutableAttributedString

        let labelString = self.text! as NSString


        for text in array {

            let rang = labelString.range(of: text as! String)

            attributed.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: rang)
            attributed.addAttribute(NSAttributedString.Key.font, value: font, range: rang)


        }

        self.attributedText = attributed


    }

    
    /// 修改文本中指定范围的字体大小颜色
    /// - Parameters:
    ///   - range: 指定范围
    ///   - font: 字体大小
    ///   - color: 字体颜色
    func changeCharacter(range:NSRange ,font:UIFont?,color:UIColor?)  {

        let attributed:NSMutableAttributedString = self.attributedText?.mutableCopy() as! NSMutableAttributedString

        attributed.addAttribute(NSAttributedString.Key.foregroundColor, value: color ?? self.textColor as UIColor, range: range)
        attributed.addAttribute(NSAttributedString.Key.font, value: font ?? self.font as UIFont, range: range)


        self.attributedText = attributed

    }
    
    
    /// 添加横线给文本
    /// - Parameter text: 指定文本
    func addlineToText(text:String)  {


        let labelString = self.text! as NSString

        let rang = labelString.range(of: text)

        let attributed:NSMutableAttributedString = self.attributedText?.mutableCopy() as! NSMutableAttributedString

        attributed.addAttribute(.underlineStyle, value: NSUnderlineStyle.single, range: rang)

        self.attributedText = attributed


    }
    

}

