//
//  JMButtonExtension.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/23.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

public extension UIButton{


    func changeCharacter(text:String, font:UIFont?, color:UIColor?)  {


        let labelString = self.currentTitle! as NSString

        let rang = labelString.range(of: text)

        if rang.location == NSNotFound {

            return
        }


        let attri:NSAttributedString = (self.attributedTitle(for: .normal) ?? NSAttributedString())!

        if self.attributedTitle(for: .normal) == nil {

            return
        }

        let attributed:NSMutableAttributedString = attri.mutableCopy() as! NSMutableAttributedString


        if color != nil {

            attributed.addAttributes([NSAttributedString.Key.foregroundColor:UIColor.red], range:rang)


        }

        if font != nil{

            attributed.addAttributes([NSAttributedString.Key.font:UIFont.systemFont(ofSize: 14)], range: rang)

        }

        self.setAttributedTitle(attributed, for: .normal)

    }



}


