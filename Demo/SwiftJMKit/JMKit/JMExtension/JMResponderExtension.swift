//
//  JMResponderExtension.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/23.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

public extension UIResponder{

    func getWindowTopController() -> Any {

        var rootVC = UIApplication.shared.keyWindow?.rootViewController

        while (rootVC!.isKind(of: UITabBarController.classForCoder())) {

            let  next:UITabBarController = rootVC as! UITabBarController
            rootVC = next.selectedViewController

        }

        while (rootVC!.isKind(of: UINavigationController.classForCoder())) {

            let  next:UINavigationController = rootVC as! UINavigationController

            rootVC = next.topViewController
        }

        return rootVC!

    }

    func nextResponder(currentView:UIResponder)->UIViewController{
        var vc:UIResponder = currentView
        while vc.isKind(of: UIViewController.self) != true {
            vc = vc.next!
        }
        return vc as! UIViewController
    }

}

