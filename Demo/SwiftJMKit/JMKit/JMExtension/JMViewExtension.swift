//
//  JMViewExtension.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/22.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit
//import SnapKit


public extension UIView {
    
    // 私有的结构体： 这个类扩展下所有新添加属性的关联值的keys
    private struct AssociateKeys {
        
        static var JMCAGradientViewKey = "toNewPropertyKey"
    }
    
    // MARK: 渐变相关

    var jm_GradientView:JMCAGradientLayerView?{
        
        // 新添加属性的set方法
        set(value) {
            
            objc_setAssociatedObject(self, &AssociateKeys.JMCAGradientViewKey, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
        
        // 新添加属性的get方法
        get{
            
            return objc_getAssociatedObject(self, &AssociateKeys.JMCAGradientViewKey) as? JMCAGradientLayerView
        }

    }
    
    
    /// 生成渐变色层
    /// - Parameters:
    ///   - colors: 渐变颜色数组
    ///   - startPoint: 开始点
    ///   - endPoint: 结束点
    ///   - Location: 颜色变化区间
    /// - Returns:
    class func CAGradient(colors:Array<CGColor>,startPoint:CGPoint,endPoint:CGPoint,Location:Array<NSNumber>?) -> CAGradientLayer {
        
        let gl = CAGradientLayer()
        gl.startPoint = startPoint
        gl.endPoint = endPoint
        gl.colors = colors
        gl.locations = Location
        
        return gl

    }
    
    func initCustomCAGraient(layer:CAGradientLayer) {
        
        if self.jm_GradientView == nil {
            self.jm_GradientView = JMCAGradientLayerView()
            self.jm_GradientView!.isUserInteractionEnabled = false
        }

        self.jm_GradientView?.jm_changeCALayer = layer
        
        if !layer.frame.equalTo(CGRect.zero) {
            self.jm_GradientView?.frame = layer.frame
        }
        
        self.insertSubview(self.jm_GradientView!, at: 0)
        
    }
    
    func initDefalutCAGraient(layer:CAGradientLayer) {
        
        self.initCustomCAGraient(layer: layer)
        
        self.jm_GradientView!.snp.makeConstraints { make in
            
            make.left.right.top.bottom.equalTo(0)
            
        }
    }
    
    func changeCustomCAGraient(layer:CAGradientLayer) {
        
        self.jm_GradientView?.jm_changeCALayer = layer
        self.jm_GradientView?.setNeedsDisplay()
        
    }
    
    func removeCustomCAGrdient() {
        
        self.jm_GradientView?.removeFromSuperview()
        
    }
    
    // MARK: 视图相关设置
    
    func jm_x() -> CGFloat {
        
        return self.frame.origin.x
    }
    
    func setJm_x(x:CGFloat) {
        
        self.frame = CGRect.init(x: x, y: self.frame.origin.y, width: self.frame.size.width, height: self.frame.size.height)
    }
    
    func jm_y() -> CGFloat {
        
        return self.frame.origin.y
    }
    
    func setJm_y(y:CGFloat) {
        
        self.frame = CGRect.init(x: self.frame.origin.x, y: y, width: self.frame.size.width, height: self.frame.size.height)
    }
    
    func jm_width() -> CGFloat {
        
        return self.frame.size.width
    }
    
    func setJm_width(width:CGFloat) {
        
        self.frame = CGRect.init(x: self.frame.origin.x, y: self.frame.origin.y, width: width, height: self.frame.size.height)
    }
    
    func jm_height() -> CGFloat {
        
        return self.frame.size.height
    }
    
    func setJm_height(height:CGFloat) {
        
        self.frame = CGRect.init(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.size.width, height:height )
    }
    
    func jm_bottom() -> CGFloat {
        
        return self.frame.origin.y + self.frame.size.height
    }
    
    func setJm_bottom(bottom:CGFloat) {
        
        self.frame = CGRect.init(x: self.frame.origin.x, y: bottom - self.jm_height(), width: self.frame.size.width, height:self.frame.size.height)
    }
    
    func jm_right() -> CGFloat {
        
        return self.frame.origin.x + self.frame.size.width
    }
    
    func setJm_right(right:CGFloat) {
        
        self.frame = CGRect.init(x: right - self.jm_width(), y: self.frame.origin.y, width: self.frame.size.width, height:self.frame.size.height)
    }
    
    func jm_frameOrigin() -> CGPoint {
        
        return self.frame.origin
    }
    
    func setJm_frameOrigin(origin:CGPoint) {
        
        self.frame = CGRect.init(origin: origin, size: self.frame.size)
        
    }
    
    func jm_boundSize() -> CGSize {
        
        return self.bounds.size
    }
    
    func setJm_boundSize(size:CGSize) {
        
        self.bounds = CGRect.init(origin: self.bounds.origin, size: size)
        
    }
    
    func removeAllSubviews()  {
        
        self.subviews.forEach{ view in
            
            view.removeFromSuperview()
        }
    }
    
    func removeKindClass(aClass:AnyClass) {
        
        self.subviews.forEach{ view in
            
            if view.isKind(of: aClass){
                
                view.removeFromSuperview()

            }
        }

    }
    
    //设置圆角
    func setCornerRadius(radius:CGFloat)  {

        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
    }

    func setCornerValueSize(size:CGSize , boundSize:CGSize,rectCorner:UIRectCorner)  {

        let boundRect:CGRect = CGRect(origin: CGPoint.zero, size: boundSize)

        let maskPath:UIBezierPath = UIBezierPath(roundedRect: boundRect, byRoundingCorners:rectCorner, cornerRadii: size)

        let maskLayer = CAShapeLayer.init()

        maskLayer.frame = boundRect
        maskLayer.path = maskPath.cgPath

        self.layer.mask = maskLayer

    }

    func setBorderWidth(width:CGFloat, color:UIColor)  {

        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
    }

    func addline(frame:CGRect) {

        let layer:CALayer = CALayer.init()
        layer.backgroundColor =  UIColor.colorWithHexString(color: "EAEAEA").cgColor
        layer.frame = frame
        self.layer.addSublayer(layer)

    }

    func addLine(frame:CGRect,color:UIColor)  {

        let layer:CALayer = CALayer.init()
        layer.backgroundColor = color.cgColor
        layer.frame = frame
        self.layer.addSublayer(layer)

    }

    func addLine(frame:CGRect,color:UIColor,alpha:CGFloat) {

        let layer:CALayer = CALayer.init()
        layer.backgroundColor = color.cgColor
        layer.frame = frame
        layer.opacity = Float(1-alpha)
        self.layer.addSublayer(layer)

    }

    func addShadow(shadowColor:UIColor,opacity:Float,shadowRaius:CGFloat,offsetSize:CGSize,cornerRadius:CGFloat)  {

        self.layer.shadowColor = shadowColor.cgColor
        // 阴影透明度，默认0
        self.layer.shadowOpacity = opacity
        // 阴影半径，默认3
        self.layer.shadowRadius = shadowRaius
        // 阴影偏移，默认(0, -3)
        self.layer.shadowOffset = offsetSize

        self.layer.cornerRadius = cornerRadius

    }

    func addShadow(shadowColor:UIColor,opacity:Float,path:UIBezierPath,offsetSize:CGSize,cornerRadius:CGFloat)  {

        self.layer.shadowColor = shadowColor.cgColor;
        self.layer.shadowOpacity = opacity;
        self.layer.shadowPath = path.cgPath;
        self.layer.shadowOffset = offsetSize;
        self.layer.cornerRadius = cornerRadius;

    }




}

public class JMCAGradientLayerView:UIView{
    
    var _jm_changeCALayer = CAGradientLayer()

    public var jm_changeCALayer:CAGradientLayer?
    {
        
        get {
            return _jm_changeCALayer
        }
        set {
            
            if _jm_changeCALayer != newValue {
                
                _jm_changeCALayer .removeFromSuperlayer()
                
            }
            
            _jm_changeCALayer = newValue ?? CAGradientLayer()
            self.layer .insertSublayer(_jm_changeCALayer, at: 0)

        }
    }
    
    public override func draw(_ rect: CGRect) {
        
        super.draw(rect)
        
        self.jm_changeCALayer?.frame = self.bounds
        
    }
    
    
    
  
    
    
}





