//
//  JMColorExtension.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/22.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

//字体颜色处理
public extension UIColor {
    
    class func colorWithHexString(color:String) -> UIColor {

        var cString = color.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased();
        if (cString.count < 6) {
            return UIColor.clear
        }

        // 判断前缀

        let index = cString.index(cString.startIndex, offsetBy: 2)
        let index2 = cString.index(cString.startIndex, offsetBy: 1)

        if cString .hasPrefix("0X") {

            cString = String(cString.suffix(from: index))
        }
        if cString .hasPrefix("#") {

            cString = String(cString.suffix(from: index2))
        }

        if (cString.count < 6) {
            return UIColor.clear
        }

        let rStartIndex = cString.startIndex
        let rEndIndex = cString.index(cString.startIndex, offsetBy: 2)
        let subR = cString[rStartIndex..<rEndIndex]

        let gStartIndex = cString.index(cString.startIndex, offsetBy: 2)
        let gEndIndex = cString.index(cString.startIndex, offsetBy: 4)
        let subG = cString[gStartIndex..<gEndIndex]


        let bStartIndex = cString.index(cString.startIndex, offsetBy: 4)
        let bEndIndex = cString.index(cString.startIndex, offsetBy: 6)
        let subB = cString[bStartIndex..<bEndIndex]


        var r :UInt32 = 0x0;
        var g :UInt32 = 0x0;
        var b :UInt32 = 0x0;
        Scanner.init(string: String(subR)).scanHexInt32(&r);
        Scanner.init(string: String(subG)).scanHexInt32(&g);
        Scanner.init(string: String(subB)).scanHexInt32(&b);
        return UIColor.init(red: CGFloat(r)/255.0, green: CGFloat(g)/255.0, blue: CGFloat(b)/255.0, alpha: 1);


    }

}

