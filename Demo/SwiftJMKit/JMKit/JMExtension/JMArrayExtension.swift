//
//  JMArrayExtension.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/22.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit
import SnapKit


public extension NSArray{
    
    
    /// 设置竖直方向的约束数组(数组内的元素需为UIView)
    /// - Parameters:
    ///   - fixSpacing: item之间的间隙
    ///   - leadSpacing: 第一个视图和父视图的顶部间隙
    ///   - tailSpacing: 最后一个视图和父视图的底部间隙
    ///   - leftSpacing: 所有item左边离父视图左边的间隙
    ///   - rightSpacing: 所有item右边离父视图右边的间隙
    func makeVerticalDirection(fixSpacing:CGFloat,leadSpacing:CGFloat, tailSpacing:CGFloat,leftSpacing:CGFloat,rightSpacing:CGFloat) {
        
        for i in 0...self.count {
            
            
            let subView:UIView = self[i] as! UIView
            
            subView.snp.makeConstraints { make in
                
                make.left.equalTo(leftSpacing)
                make.right.equalTo(-rightSpacing)
                
            }
            
            if i == 0 {
                
                subView.snp.makeConstraints { make in
                    
                    make.top.equalTo(leadSpacing)
                    
                }

                
            }
            
            if i == self.count - 1 {
                
                subView.snp.makeConstraints { make in
                    
                    make.bottom.equalTo(-tailSpacing)
                    
                }

                
            }
            
            if i > 0 {
                
                let lastView:UIView = self[i-1] as! UIView
                
                subView.snp.makeConstraints { make in
                    
                    make.top.equalTo(lastView.snp_bottom).offset(fixSpacing)
                    
                }

                
            }


            
        }
        
    }
    
    
    /// 设置水平方向的约束数组
    /// - Parameters:
    ///   - fixSpacing: item之间的间隙
    ///   - leadSpacing: 第一个视图和父视图的左边间隙
    ///   - tailSpacing: 最后一个视图和父视图的右边间隙
    ///   - topSpacing: 所有item顶部离父视图顶部的间隙
    ///   - bottomSpacing: 所有item底部离父视图底部的间隙
    func makeHorizontalDirection(fixSpacing:CGFloat,leadSpacing:CGFloat, tailSpacing:CGFloat,topSpacing:CGFloat,bottomSpacing:CGFloat) {
        
        for i in 0...self.count {
            
            
            let subView:UIView = self[i] as! UIView
            
            subView.snp.makeConstraints { make in
                
                make.top.equalTo(topSpacing)
                make.bottom.equalTo(-bottomSpacing)
                
            }
            
            if i == 0 {
                
                subView.snp.makeConstraints { make in
                    
                    make.left.equalTo(leadSpacing)
                    
                }

                
            }
            
            if i == self.count - 1 {
                
                subView.snp.makeConstraints { make in
                    
                    make.right.equalTo(-tailSpacing)
                    
                }

                
            }
            
            if i > 0 {
                
                let lastView:UIView = self[i-1] as! UIView
                
                subView.snp.makeConstraints { make in
                    
                    make.left.equalTo(lastView.snp_right).offset(fixSpacing)
                    
                }

                
            }


            
        }
        
    }


    
}





