//
//  JMStringExtension.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/22.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

public extension String{

    // MARK: 字符串功能相关
    
    //使用mutating后可修改属性的值
    /// 清除空格
    mutating func clearWhiteSpace()  {

        let whitespace = NSCharacterSet.whitespaces
        self =  self.trimmingCharacters(in: whitespace)

    }
    
    // MARK: 字符串截取相关
    
    /// 使用下标获取范围内字符串string[index..<index]
    subscript (r: Range<Int>) -> String {
        get {
            let startIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
            let endIndex = self.index(self.startIndex, offsetBy: r.upperBound)
            return String(self[startIndex..<endIndex])
        }
    }
    
    /// 使用下标获取范围内字符串string[index,length]
    subscript (index:Int , length:Int) -> String {
        get {
            let startIndex = self.index(self.startIndex, offsetBy: index)
            let endIndex = self.index(startIndex, offsetBy: length)
            return String(self[startIndex..<endIndex])
        }
    }
    
    func getIndexString(i:Int) -> String{
        
        let startIndex = self.index(self.startIndex, offsetBy: i)
        let endIndex = self.index(startIndex, offsetBy: 1)
        return String(self[startIndex..<endIndex])
    }


    /// 截取 从头到i位置
    /// - Parameter to:结束索引
    /// - Returns: 返回截取后的字符串
    func substring(to:Int) -> String{
        return self[0..<to]
    }

    /// 截取 从i到尾部
    /// - Parameter from: 开始索引
    /// - Returns: 返回截取后的字符串
    func substring(from:Int) -> String{
        return self[from..<self.count]
    }
    
    // MARK: 字符串判断相关

    ///是不是纯数字
    /// - Returns: 返回判断结果
    func isPurnInt() -> Bool {

        if self.count == 0 {

            return false
        }

        let scan: Scanner = Scanner(string: self)

        var val:Int = 0

        return scan.scanInt(&val) && scan.isAtEnd

    }

}

