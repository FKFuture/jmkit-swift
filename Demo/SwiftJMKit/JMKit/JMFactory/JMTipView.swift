//
//  JMTipView.swift
//  MCN
//
//  Created by JM on 2020/4/1.
//  Copyright © 2020 xinlianyun. All rights reserved.
//

import UIKit

class JMTipView: UIView {
    
    enum  JMTipStatus{  //提示方式
        case titleTipStatus        //文本提示
        case imageTipStatus       //图片或背景颜色提示
        case customTipStatus     //自定义视图提示

    }

    enum  JMTipDirection{  //提示方向，即提示文本和内容文本的相对位置
        case tipLeftDirection        //提示文本在左，内容文本在右边，默认
        case tipRightDirection       //提示文本在右，内容文本在左边
        case tipTopDirection         //提示文本在上，内容文本在下
        case tipBottomDirection      //提示文本在下，内容文本在上

    }



    let tipImageView = UIImageView()
    let titleLabel = JMLabel()
    var tipCustomView:UIView?  //自定义提示视图
    var imageTitleMargin:CGFloat = 0.0
    var TitleMargin:CGFloat = 0.0      //提示文本和内容文本的间距
    var imageSize:CGSize = CGSize()
    let tipLabel = JMLabel()
    var tipStatus = JMTipStatus.imageTipStatus
    var tipDirection:JMTipDirection? {

        get {
            return _tipDirection
        }
        set {
            _tipDirection = newValue!
            self.updateTipDirection()
        }

    }

    var _tipDirection:JMTipDirection?


    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    convenience init(tipTitle:String,titleMargin:CGFloat,content:String,titleFont:CGFloat,titleColor:UIColor){

        self.init()

        self.TitleMargin = titleMargin


        self.tipLabel.text = tipTitle

        self.titleLabel.text = content
        self.titleLabel.font = UIFont.systemFont(ofSize: titleFont)
        self.titleLabel.textColor = titleColor
        self.titleLabel.numberOfLines = 0
        self.tipStatus = JMTipStatus.titleTipStatus

        self.creatSubview()


    }

    func changeTipLabel(font:UIFont,color:UIColor)  {

        tipLabel.textColor = color
        tipLabel.font = font

    }

    convenience init(customView:UIView,titleMargin:CGFloat,content:String,titleFont:CGFloat,titleColor:UIColor){

        self.init()

        self.TitleMargin = titleMargin

        self.titleLabel.text = content
        self.titleLabel.font = UIFont.systemFont(ofSize: titleFont)
        self.titleLabel.textColor = titleColor
        self.titleLabel.numberOfLines = 0
        self.tipStatus = JMTipStatus.customTipStatus
        self.tipCustomView = customView

        self.creatSubview()


    }




    convenience init(tipColor:UIColor,alpa:CGFloat,imageSize:CGSize,titleImageMargin:CGFloat,content:String,titleFont:CGFloat,titleColor:UIColor){

        self.init()

        self.imageTitleMargin = titleImageMargin
        self.imageSize = imageSize



        self.tipImageView.backgroundColor = tipColor
        self.tipImageView.layer.cornerRadius = imageSize.width/2
        self.tipImageView.layer.masksToBounds = true
        self.tipImageView.alpha = alpa
        self.titleLabel.text = content
        self.titleLabel.font = UIFont.systemFont(ofSize: titleFont)
        self.titleLabel.textColor = titleColor
        self.titleLabel.numberOfLines = 0
        self.tipStatus = JMTipStatus.imageTipStatus

        self.creatSubview()



    }


    convenience init(tipImage:UIImage,imageSize:CGSize,titleImageMargin:CGFloat,content:String,titleFont:CGFloat,titleColor:UIColor){

        self.init()

        self.imageTitleMargin = titleImageMargin
        self.imageSize = imageSize


        self.tipImageView.image = tipImage
        self.titleLabel.text = content
        self.titleLabel.font = UIFont.systemFont(ofSize: titleFont)
        self.titleLabel.textColor = titleColor
        self.titleLabel.numberOfLines = 0

        self.tipStatus = JMTipStatus.imageTipStatus
        self.creatSubview()


    }

    func creatSubview() {


        self.addSubview(tipImageView)
        self.addSubview(titleLabel)
        self.addSubview(tipLabel)
        self.addSubview(tipCustomView ?? UIView())

        if tipStatus == .imageTipStatus {

            tipImageView.snp.makeConstraints { (make) in

                make.left.equalToSuperview()
                make.top.equalTo(4)
                make.size.equalTo(self.imageSize)

            }

            titleLabel.snp.makeConstraints { (make) in

                make.left.equalTo(tipImageView.snp_right).offset(self.imageTitleMargin)
                make.top.equalToSuperview()
                make.right.equalToSuperview()
                make.bottom.equalToSuperview()

            }


        }else if tipStatus == .titleTipStatus{

            tipLabel.snp.makeConstraints { (make) in

                make.left.equalToSuperview()
                make.top.equalToSuperview()
            }

            titleLabel.snp.makeConstraints { (make) in

                make.left.equalTo(tipLabel.snp_right).offset(self.TitleMargin)
                make.top.equalToSuperview()
                make.right.equalToSuperview()
                make.bottom.equalToSuperview()

            }



        }else if tipStatus == .customTipStatus{


            tipCustomView?.snp.makeConstraints({ (make) in

                make.left.equalToSuperview()
                make.top.equalToSuperview()

            })

            titleLabel.snp.makeConstraints { (make) in

                make.left.equalTo(tipCustomView!.snp_right).offset(self.TitleMargin)
                make.top.equalToSuperview()
                make.right.equalToSuperview()
                make.bottom.equalToSuperview()

            }




        }



    }


    func updateTipDirection() {

        if tipDirection == .tipLeftDirection {

            tipLabel.snp.remakeConstraints { (make) in

                make.left.equalToSuperview()
                make.top.equalToSuperview()
            }

            titleLabel.snp.remakeConstraints { (make) in

                make.left.equalTo(tipLabel.snp_right).offset(self.TitleMargin)
                make.top.equalToSuperview()
                make.right.equalToSuperview()

            }

        }

        if tipDirection == .tipRightDirection {

            tipLabel.snp.remakeConstraints { (make) in

                make.right.equalToSuperview()
                make.top.equalToSuperview()
            }

            titleLabel.snp.remakeConstraints { (make) in

                make.right.equalTo(tipLabel.snp_left).offset(-self.TitleMargin)
                make.top.equalToSuperview()
                make.left.equalToSuperview()

            }

        }

        if tipDirection == .tipTopDirection {

            tipLabel.snp.remakeConstraints { (make) in

                make.top.equalToSuperview()
                make.centerX.equalToSuperview()

            }

            titleLabel.snp.remakeConstraints { (make) in

                make.top.equalTo(tipLabel.snp_bottom).offset(self.TitleMargin)
                make.bottom.equalToSuperview()
                make.centerX.equalToSuperview()


            }

        }

        if tipDirection == .tipBottomDirection {

            tipLabel.snp.remakeConstraints { (make) in

                make.bottom.equalToSuperview()
                make.centerX.equalToSuperview()

            }

            titleLabel.snp.remakeConstraints { (make) in

                make.bottom.equalTo(tipLabel.snp_top).offset(-self.TitleMargin)
                make.top.equalToSuperview()
                make.centerX.equalToSuperview()


            }

        }


    }

}
