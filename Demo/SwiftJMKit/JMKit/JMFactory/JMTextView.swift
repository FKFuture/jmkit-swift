//
//  JMTextView.swift
//  JMKit
//
//  Created by JM on 2019/8/30.
//  Copyright © 2019 YQ. All rights reserved.
//

import UIKit

public class JMTextView: UITextView,UITextViewDelegate {

    let  maxTipLabel = JMLabel()
    let  placeHolderLabel = JMLabel()
    var  maxTextCount = 0
    var  placeHolder = ""
    var  placeHolderColor = UIColor()
    var  placeHolderFont = UIFont()


   public convenience init(frame:CGRect,placeHoder:String,placeColor:UIColor,placeFont:UIFont){

        self.init()

        self.placeHolder = placeHoder
        self.placeHolderColor = placeColor
        self.placeHolderFont = placeFont

        self.creatSubviews()


        self.initData()

    }

    public convenience init(placeHoder:String,placeColor:UIColor,placeFont:UIFont){

        self.init()

        self.placeHolder = placeHoder
        self.placeHolderColor = placeColor
        self.placeHolderFont = placeFont

        self.creatSubviews()


    }


    func creatSubviews()  {

        placeHolderLabel.text = placeHolder
        placeHolderLabel.textColor = placeHolderColor
        placeHolderLabel.font = placeHolderFont
        placeHolderLabel.numberOfLines = 0


        maxTipLabel.textColor = placeHolderColor
        maxTipLabel.font = placeHolderFont

        self.addSubview(maxTipLabel)
        self.addSubview(placeHolderLabel)

        placeHolderLabel.snp.makeConstraints { (make) in

            make.left.equalTo(10)
            make.top.equalTo(10)

        }

    }

    public override func layoutSubviews() {

        super.layoutSubviews()

        placeHolderLabel.snp.remakeConstraints { (make) in

            make.left.equalTo(10)
            make.top.equalTo(10)
            make.width.equalTo(self.bounds.width - 20)


        }




    }

    func initData()  {


        self.delegate = self
        self.textContainerInset = UIEdgeInsets(top: 16, left: 17, bottom: 20, right: 17)

    }

    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        if text == "\n" {

            return false
        }

        return true
    }

    public func textViewDidChange(_ textView: UITextView) {


        if textView.text.count > 0 {

            placeHolderLabel.isHidden = true
        }else{

            placeHolderLabel.isHidden = false
        }


        NotificationCenter.default.post(name: Notification.Name(rawValue: "TextChange"), object: nil)


        //发出字数通知

        if maxTextCount > 0 {

            self.limitString(maxLength: maxTextCount)

        }


    }

    //监听到文本复制
    public func textViewDidChangeSelection(_ textView: UITextView) {

        self.placeHolderLabel.isHidden = true

        if textView.text.count > self.maxTextCount && maxTextCount > 0{

            self.limitString(maxLength: maxTextCount)
        }

        NotificationCenter.default.post(name: Notification.Name(rawValue: "TextChange"), object: nil)


    }

    func addLink(text:String)  {

        let attributed:NSMutableAttributedString = self.attributedText.mutableCopy() as! NSMutableAttributedString

        let labelString = self.text! as NSString

        let rang = labelString.range(of: text)

        attributed.addAttribute(.link, value: text, range: rang)

        self.attributedText = attributed

    }

    func changeCharacter(with text:String, font:UIFont, color:UIColor)  {


        let labelString = self.text! as NSString

        let rang = labelString.range(of: text)

        let attributed:NSMutableAttributedString = self.attributedText?.mutableCopy() as! NSMutableAttributedString

        attributed.addAttributes([NSAttributedString.Key.foregroundColor:color ,NSAttributedString.Key.font:font ], range: rang)


        self.attributedText = attributed

    }


    func changeCharacter(withArray array:NSArray ,font:UIFont,color:UIColor)  {

        let attributed:NSMutableAttributedString = self.attributedText?.mutableCopy() as! NSMutableAttributedString

        let labelString = self.text! as NSString


        for text in array {

            let rang = labelString.range(of: text as! String)

            attributed.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: rang)
            attributed.addAttribute(NSAttributedString.Key.font, value: font, range: rang)

        }

        self.attributedText = attributed


    }




    func limitString(maxLength:NSInteger)  {



    }




}
