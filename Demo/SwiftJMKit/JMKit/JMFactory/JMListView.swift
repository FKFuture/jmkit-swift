//
//  JMListView.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/24.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

public class JMListView: UIView {
    
    var dataArr:NSArray?
    var leftPadding:CGFloat = 0
    var rightPadding:CGFloat = 0
    var itemMargin:CGFloat = 0
    var itemWidth:CGFloat = 0
    var itemHeight:CGFloat = 0
    var rowCount:NSInteger = 0
    var colunmnCount:NSInteger = 0
    var scrollView:JMScrollView?
    var listDelegate:JMListActionDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public convenience init(actionList:NSArray,rowCount:NSInteger,column:NSInteger,leftPadding:CGFloat,rightPadding:CGFloat,itemMargin:CGFloat,itemWidth:CGFloat,itemHeight:CGFloat,listDelegate:JMListActionDelegate?){
        
        self.init()
        
        self.dataArr = actionList
        self.rowCount = rowCount
        self.colunmnCount = column
        self.leftPadding = leftPadding
        self.rightPadding = rightPadding
        self.itemMargin = itemMargin
        self.itemWidth = itemWidth
        self.itemHeight = itemHeight
        self.listDelegate = listDelegate
        
        self.createListViews()
        
    }

    
    
    
    func createListViews()  {
        
        self.scrollView = JMScrollView.init()
        self.scrollView?.contentScrollStatus = .JMHorizontalScrollStatus
        self.scrollView?.showsHorizontalScrollIndicator = false
        self.addSubview(self.scrollView!)
        
        self.scrollView?.snp.makeConstraints({ make in
            
            make.left.right.top.bottom.equalTo(0)
        })
        
        let pageItemCount = self.rowCount * self.colunmnCount
        
        guard self.dataArr?.count ?? 0 > 0 else {
            return
        }

        guard pageItemCount > 0 else {
            return
        }
        
        var pageCount = self.dataArr!.count/pageItemCount
        
        let remainCount = self.dataArr!.count % pageItemCount
        
        if remainCount > 0 {

            pageCount += 1
        }
        
        let pageViewArray = NSMutableArray.init()
        
        let pageWidth = self.leftPadding + self.rightPadding + self.itemWidth * CGFloat(self.colunmnCount) + CGFloat(self.colunmnCount - 1)*self.itemMargin
        
        let pageHeight = self.itemHeight * CGFloat(self.rowCount) + CGFloat(self.rowCount - 1)*self.itemMargin
        
        
        for i in 0..<pageCount {
            
            let pageView = UIView.init()
            self.scrollView?.addSubview(pageView)
            
            pageViewArray.add(pageView)
            
            pageView.snp.makeConstraints { make in
                
                make.width.equalTo(self.snp_width).offset(0)
                make.width.equalTo(pageWidth)
                make.height.equalTo(pageHeight)
                
                
            }
            
            let itemViewArray = NSMutableArray.init()
            var limitCount = pageItemCount
            
            if i == pageCount - 1 {
                limitCount = self.dataArr!.count - (pageCount - 1)*pageItemCount
            }
            
            for j in 0..<limitCount {
                
                let currentRow = j/self.colunmnCount
                let currentColumn = j % self.colunmnCount
                
                let index = i * pageItemCount + j
                
                if self.listDelegate != nil {
                    
                    let view = (self.listDelegate?.getItemView(page: i, row: currentRow, column: currentColumn, index: index))!
                    
                    pageView.addSubview(view)
                    
                    itemViewArray.add(view)
                    
                    view.snp.makeConstraints { make in
                        
                        make.left.equalTo(self.leftPadding + (self.itemWidth + self.itemMargin)*CGFloat(currentColumn))
                        make.top.equalTo((self.itemHeight + self.itemMargin)*CGFloat(currentRow))
                        make.width.equalTo(self.itemWidth)
                        make.height.equalTo(self.itemHeight)
                        
                    }
                    
                    

                }
                
                
                
                
            }
            
            
        }
        
        pageViewArray.makeHorizontalDirection(fixSpacing: 0, leadSpacing: 0, tailSpacing: 0, topSpacing: 0, bottomSpacing: 0)
        
        
        
    }
    
    
}

public protocol JMListActionDelegate: NSObjectProtocol{
    
    
    /// 返回指定页指定列指定行的视图
    /// - Parameters:
    ///   - page: 当前页
    ///   - row: 当前行
    ///   - column: 当前列
    ///   - index: 当前索引
    func getItemView(page:NSInteger,row:NSInteger,column:NSInteger,index:NSInteger) ->UIView
    
}
