//
//  JMTextField.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/24.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

public class JMTextField: UITextField {
    
    
    ///输入框显示的最大字符数（默认是0，不限制字数）
    public var maxCharacterCount:Int = 0
    
    ///输入框内容距离右边间距
    public var rightMargin:CGFloat = 0
    
    ///输入框内容距离左边间距
    public var leftMargin:CGFloat = 0
    
    ///提示文本距离顶部间距
    private var placeholderMarginY:CGFloat = 0

    var _placeholderColor:UIColor?
    
    /// 提示文本字体颜色
    public var placeholderColor:UIColor?{
        
        get {
            return _placeholderColor
        }
        set {
            _placeholderColor = newValue
            self.changePlaceholderColor(placeholderColor: _placeholderColor)

        }
    }
    
    private   func changePlaceholderColor(placeholderColor:UIColor?)  {
        
        
        guard self.placeholder?.count ?? 0 > 0 else {
            
            return
        }
        
        guard placeholderColor != nil else {
            
            return
        }
        
        let attributed:NSMutableAttributedString = self.attributedPlaceholder?.mutableCopy() as! NSMutableAttributedString

        attributed.addAttributes([NSAttributedString.Key.foregroundColor:placeholderColor!], range: NSRange(location: 0, length: self.placeholder?.count ?? 0))

        self.attributedPlaceholder = attributed;


    }

    var _placeholderFont:UIFont?
    
    /// 提示文本字体大小
    public var placeholderFont:UIFont?{
        
        get {
            return _placeholderFont
        }
        set {
            _placeholderFont = newValue
            self.changePlaceholderFont(placeholderFont: _placeholderFont)

        }

    }
    
    private   func changePlaceholderFont(placeholderFont:UIFont?)  {
        
        guard self.placeholder?.count ?? 0 > 0 else {
            
            return
        }
        
        guard placeholderColor != nil else {
            
            return
        }

        let attributed:NSMutableAttributedString = self.attributedPlaceholder?.mutableCopy() as! NSMutableAttributedString

        attributed.addAttributes([NSAttributedString.Key.font:placeholderFont!], range: NSRange(location: 0, length: self.placeholder?.count ?? 0))

        self.attributedPlaceholder = attributed;


    }

    /// 是否禁止复制，默认是false,不禁止
    public var forbidCopy:Bool = false
    
    ///是否禁止选择，默认是false,不禁止
    public var forbidSelete:Bool = false
    
    ///是否禁止多选，默认是false,不禁止
    public var forbidAllSelete:Bool = false

    override init(frame: CGRect) {
        
        super.init(frame: frame)

        self.addEditChangeObserver()

    }

    //swift中规定:重写控件的init方法，必须重写 init?(coder aDecoder: NSCoder)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience  init(frame: CGRect,textFontSize:CGFloat,placehoder:String) {

        self.init(frame: frame)
        self.font = UIFont.systemFont(ofSize: textFontSize)
        self.placeholder = placehoder

    }

    func addEditChangeObserver()  {

        self.addTarget(self, action:#selector(limitString), for: .editingChanged)
    }

    //设置提示文字区域
    public override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        
        return CGRect(x: bounds.origin.x+self.leftMargin, y: bounds.origin.y+self.placeholderMarginY, width: bounds.size.width-self.leftMargin-self.rightMargin, height: bounds.size.height-self.placeholderMarginY)

    }

    //设置编辑文本光标的位置（点击输入框就会调用,光标位置确定了，输入框的文本显示区域初始位置也就确定了）
    public override func editingRect(forBounds bounds: CGRect) -> CGRect {
        
        
        let frame:CGRect = CGRect(x: bounds.origin.x+self.leftMargin, y:bounds.origin.y+self.placeholderMarginY , width: bounds.size.width - self.rightMargin - self.leftMargin, height: bounds.size.height - self.placeholderMarginY)

        return frame

    }


    //设置文本显示的区域
    public override func textRect(forBounds bounds: CGRect) -> CGRect {
        
        return self.editingRect(forBounds: bounds)
    }
    
    public override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {

        if action == #selector(paste(_:)) {
            return !forbidCopy
        }

        if action == #selector(select(_:)) {
            return !forbidSelete
        }

        if action == #selector(selectAll(_:)) {
            return !forbidAllSelete
        }

        return super.canPerformAction(action, withSender: sender)


    }


    @objc func limitString()  {
        
        if (self.maxCharacterCount <= 0 ) {

            return
        }

        let toBeString = self.text

        if (self.textInputMode?.primaryLanguage == "zh-Hans") {
            
            let selectedRange:UITextRange? = self.markedTextRange ?? UITextRange()

            let position = self.position(from: selectedRange!.start, offset: 0)


            if(position == nil){

                if (toBeString!.count > self.maxCharacterCount) {

                    let index = self.text?.index(self.text!.startIndex, offsetBy: self.maxCharacterCount)
                    self.text = String(self.text!.prefix(upTo: index!))   //(截取到某个索引)

                }

            }else{

                //有高亮的文字则暂时不限制


            }


        }else{


            if(toBeString!.count > self.maxCharacterCount){


                let index = self.text?.index(self.text!.startIndex, offsetBy: self.maxCharacterCount)
                self.text = String(self.text!.prefix(upTo: index!))   //(截取到某个索引)


            }

        }



    }







}
