//
//  JMSwitch.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/24.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

public class JMSwitch: UISwitch {
    
    var normalBackGroundColor:UIColor?
    
    public convenience init(normalBackGroundColor:UIColor?,openBackGroundColor:UIColor?,switchColor:UIColor?){
        
        self.init()
        
        //设置开关未选中背景颜色
        if normalBackGroundColor != nil {
            
            self.normalBackGroundColor = normalBackGroundColor!
            self.backgroundColor = normalBackGroundColor
        }
        
        //设置选中背景颜色
        if openBackGroundColor != nil {
            
            self.onTintColor = openBackGroundColor!
            
        }
        
        //设置开关按键颜色（选中和未选中颜色）
        if switchColor != nil {
            
            self.thumbTintColor = switchColor!
            
        }



    }

    
    public override func layoutSubviews() {
        
        super.layoutSubviews()
        
        if self.normalBackGroundColor != nil {
            
            self.layer.cornerRadius = self.frame.size.height/2
            self.layer.masksToBounds = true
        }
        
    }
    


}
