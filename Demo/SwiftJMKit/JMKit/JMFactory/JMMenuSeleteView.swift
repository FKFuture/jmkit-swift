//
//  JMMenuSeleteView.swift
//  SwiftJMKit
//
//  Created by JM on 2022/8/22.
//  Copyright © 2022 JM. All rights reserved.
//



import UIKit

class JMMenuSeleteView: UIView {
    
    weak var delegate:JMMenuSeleteViewDelegate?
    lazy var titleArray:[String] = Array<String>()
    private lazy var bgView = UIView()
    private lazy var linePath : UIBezierPath = UIBezierPath()
    private lazy var lineShape : CAShapeLayer = CAShapeLayer()
    private var imageArray:[String] = Array<String>()
    private var defalutTitleColor:UIColor = .black
    private var seleteShowColor:UIColor?
    private var itemWidth:CGFloat = 150
    private var itemHeight:CGFloat = 40
    var topMargin:CGFloat =  64
    private var padding = 15 //距离边缘的距离
    private var isRight:Bool = false //默认是显示在左边


    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(dataArray:Array<String>,imageArray:Array<String>,normalTextColor:UIColor,seleteColor:UIColor) {
        
        self.init(frame: .zero)
        titleArray = dataArray
        defalutTitleColor = normalTextColor
        seleteShowColor = seleteColor
        
        self.backgroundColor = UIColor.init(white: 0, alpha: 0.4)
        self.frame = UIScreen.main.bounds

        
        self.createPopView()
    
    }
    
    func createPopView()  {
        
        for i in 0..<titleArray.count {
            
            let imageName:String = (i<imageArray.count) ? imageArray[i] : ""
            
            let button = JMFactory.creatButton(frame: .zero, title: titleArray[i] , titleColor: defalutTitleColor, fontSize: 14, norImageName: imageName, preImageName: imageName, target: self, selector: #selector(buttonClickAction(button:)))
            button.changeImageTitleMode(mode: .ImageLeftMode, margin: 10)
            button.addLine(frame: CGRect(x: 10, y: itemHeight - 1, width: itemWidth - 20, height: 1), color: UIColor.colorWithHexString(color: "#EAEAEA"))
            button.tag = i
            button.frame = CGRect(x: 0, y: CGFloat(i)*itemHeight, width: itemWidth, height: itemHeight)
            
            bgView.addSubview(button)

            
        }
        
        
        bgView.backgroundColor = .white
        bgView.setCornerRadius(radius: 4)
        self .addSubview(bgView)
        self.layer.addSublayer(lineShape)
        
        let KWidth:CGFloat = UIScreen.main.bounds.size.width
        
        
        if isRight{
            
            bgView.frame = CGRect(x: KWidth-itemWidth-CGFloat(padding), y: topMargin, width: itemWidth, height: itemHeight * (CGFloat)(titleArray.count))

        }else{

            bgView.frame = CGRect(x: 15, y: topMargin, width: itemWidth, height: itemHeight * (CGFloat)(titleArray.count))

        }

        
    }
    
    override func layoutSubviews() {

        super.layoutSubviews()
        
        let KWidth:CGFloat = UIScreen.main.bounds.size.width

        let width = 10.0

        if isRight {

            linePath.move(to: CGPoint(x: KWidth - 20, y:topMargin))
            linePath.addLine(to: CGPoint(x: KWidth - 20-width, y: topMargin - width))
            linePath.addLine(to: CGPoint(x: KWidth - 20 - width*2, y: topMargin))

        }else{

            linePath.move(to: CGPoint(x: 27, y:topMargin))
            linePath.addLine(to: CGPoint(x: 27 + width/2, y: topMargin - width))
            linePath.addLine(to: CGPoint(x:  27 + width, y: topMargin))

        }


        linePath.close()// 闭合路径

        lineShape.frame=CGRect(x:0, y:0, width:frame.size.width, height:frame.size.height)
        lineShape.lineWidth = 1 //线宽
        lineShape.lineJoin = CAShapeLayerLineJoin.miter //线条间的样式
        lineShape.lineCap = CAShapeLayerLineCap.square //线结尾样式
        lineShape.strokeColor = UIColor.white.cgColor  //路径颜色
        lineShape.path = linePath.cgPath              //路径
        lineShape.fillColor = UIColor.white.cgColor     //填充颜色


    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        super.touchesBegan(touches, with: event)

        let touch:UITouch = (touches as NSSet).anyObject() as! UITouch     //进行类  型转化

        let touchView:UIView? = touch.view

        if touchView != bgView {

            self.removeFromSuperview()

        }


    }

    
    //按钮点击
    @objc func buttonClickAction(button:JMButton){
        
        button.isSelected = true

        self.hide()
                        
        //判断代理方法是否实现
        let isRespond:Bool = self.delegate?.responds(to: #selector(self.delegate?.menuItemSelect(index:menuView:))) as? Bool ?? false
                                                     
        if (isRespond) {

            self.delegate?.menuItemSelect(index: button.tag, menuView: self)

        }
        


    }
    
    func show(){
        
        UIApplication.shared.keyWindow?.addSubview(self)

        
    }
    
    func hide(){

        self.removeFromSuperview()


    }

}

//选中按钮代理
@objc protocol JMMenuSeleteViewDelegate:NSObjectProtocol {
    
    
    func menuItemSelect(index:NSInteger,menuView:JMMenuSeleteView)
    

}



