//
//  JMControl.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/24.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

public class JMControl: UIControl {
    
    public typealias actionBlock = () -> Void
    public var ClickAction:actionBlock?

    override init(frame: CGRect) {
        
        super.init(frame: frame)
        self.initAction()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initAction()  {
        
        self.addTarget(self, action: #selector(click), for: .touchUpInside)
        
    }
    
    @objc func click() {
        
        if  self.ClickAction != nil {

            self.ClickAction!()

        }
        
    }
    

}
