//
//  JMAutoFitLabel.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/24.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

class JMAutoFitLabel: UILabel {
    
    private var edgeInsets: UIEdgeInsets!
    
    // 从【代码】生成才会调用
    init(contentInset: UIEdgeInsets) {
        super.init(frame: .zero) // frame 随便给一个
        
        self.edgeInsets = contentInset;
        
    }
    

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // 重写textRect方法
    public override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        
        var rect: CGRect = super.textRect(
            forBounds: bounds.inset(by: edgeInsets),
            limitedToNumberOfLines: numberOfLines
        )   // 根据edgeInsets，修改绘制文字的bounds
        
        rect.origin.x -= edgeInsets.left // 调整origin的X坐标
        rect.origin.y -= edgeInsets.top // 调整origin的Y坐标
        rect.size.width += edgeInsets.left + edgeInsets.right // 调整size的width
        rect.size.height += edgeInsets.top + edgeInsets.bottom // 调整size的height
        
        return rect
    }
    
    // 重写drawText方法
    public override func drawText(in rect: CGRect) {
        
    
        super.drawText(in: rect.inset(by: edgeInsets))
    }



}
