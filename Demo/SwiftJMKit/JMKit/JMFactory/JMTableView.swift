//
//  JMTableView.swift
//  JMKit
//
//  Created by JM on 2019/12/13.
//  Copyright © 2019 xinlianyun. All rights reserved.
//

import UIKit

public class JMTableView: UITableView {

    private var jm_emptyView:UIView?
    private var jm_contentView:UIView?
    private var changeY:CGFloat = 0.0
    var noDataShowHeadView:Bool = true
    var noDataShowFootView:Bool = true


    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func setEmpetyBackgroundColor(color:UIColor){

        jm_emptyView?.backgroundColor = color

    }


    //初始化空视图
    public func initEmptyView(title:String?,FontSize:CGFloat,titleColor:UIColor?,image:UIImage?,imageTitleMargin:CGFloat,padding:CGFloat)  {

        jm_emptyView = UIView()

        let titleLabel = JMFactory.creatLabel(title: title ?? "" , titleColor:titleColor ?? UIColor.black, fontSize:FontSize )
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0

        jm_contentView = UIView()

        let imageView = UIImageView()
        imageView.image = image

        self.addSubview(jm_emptyView!)
        jm_emptyView?.addSubview(jm_contentView!)
        jm_contentView?.addSubview(titleLabel)
        jm_contentView?.addSubview(imageView)

        jm_contentView?.snp.makeConstraints { (make) in

            make.centerX.equalToSuperview()
            make.centerY.equalTo(self.snp_centerY).offset(0)

        }

        imageView.snp.makeConstraints { (make) in

            make.top.equalTo(0)
            make.centerX.equalTo(titleLabel.snp_centerX)
        }

        titleLabel.snp.makeConstraints { (make) in

            make.top.equalTo(imageView.snp_bottom).offset(imageTitleMargin)
            make.left.equalTo(padding)
            make.right.equalTo(-padding)
            make.bottom.equalTo(0)

        }


    }

    public func setVercitalLocation(value:CGFloat)  {

        changeY = value

    }

    public override func layoutSubviews() {

        super.layoutSubviews()

        //每次重绘时判断是否需要显示空视图

        var totalCount:Int = 0

        for  i in 0..<self.numberOfSections{

            let sectionCount = self.numberOfRows(inSection: i)
            totalCount += sectionCount

        }

        if totalCount > 0 {
            jm_emptyView?.isHidden = true
            //隐藏头部视图，尾部视图
            self.tableHeaderView?.isHidden = false
            self.tableFooterView?.isHidden = false


        }else{
            jm_emptyView?.isHidden = false
            jm_emptyView?.frame = self.bounds



            jm_contentView?.snp.updateConstraints({ (make) in

                make.centerY.equalTo(self.snp_centerY).offset(changeY)

            })

            guard noDataShowHeadView  else {

                self.tableHeaderView?.isHidden = true

                return
            }

            guard noDataShowFootView  else {

                self.tableFooterView?.isHidden = true

                return
            }


        }


    }


}
