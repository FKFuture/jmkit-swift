//
//  JMScrollView.swift
//  JMKit
//
//  Created by JM on 2019/12/27.
//  Copyright © 2019 JM. All rights reserved.
//

import UIKit

/**
 适用于内容宽度为固定，高度自适应计算
 */

public class JMScrollView: UIScrollView {
    
    enum JMScrollContetStatus {
        
        case JMVerticalScrollStatus
        case JMHorizontalScrollStatus
        case JMAllDirectionScrollStatus
    }

    private var contentView = UIView()
    
    var HeightLayoutContraint:NSLayoutConstraint?
    var WidthLayoutContraint:NSLayoutConstraint?
    var contentScrollStatus:JMScrollContetStatus?{
        
        didSet{
            
            self.changeContraint()
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        self.creatContentView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func creatContentView()  {

        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(self.contentView)

        let leftLayoutContraint = NSLayoutConstraint.init(item: self.contentView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0)

        let rightLayoutContraint = NSLayoutConstraint.init(item: self.contentView, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0)

        let topLayoutContraint = NSLayoutConstraint.init(item: self.contentView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)

        let BottomLayoutContraint = NSLayoutConstraint.init(item: self.contentView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)

        self.WidthLayoutContraint = NSLayoutConstraint.init(item: self.contentView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 0)
        
        self.HeightLayoutContraint = NSLayoutConstraint.init(item: self.contentView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0)


        self.addConstraint(leftLayoutContraint)
        self.addConstraint(rightLayoutContraint)
        self.addConstraint(topLayoutContraint)
        self.addConstraint(BottomLayoutContraint)
        self.addConstraint(self.WidthLayoutContraint!)
        self.addConstraint(self.HeightLayoutContraint!)
        
        self.contentScrollStatus = .JMVerticalScrollStatus


    }
    
    func changeContraint() {
        
        self.removeConstraint(self.WidthLayoutContraint!)
        self.removeConstraint(self.HeightLayoutContraint!)
        
        if self.contentScrollStatus == .JMVerticalScrollStatus {
            
            self.addConstraint(self.WidthLayoutContraint!)
        }
        
        if self.contentScrollStatus == .JMHorizontalScrollStatus {
            
            self.addConstraint(self.HeightLayoutContraint!)
        }

        
    }






}
