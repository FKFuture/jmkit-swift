//
//  JMFactory.swift
//  JMKit-0.1.0
//
//  Created by JM on 2019/8/30.
//  Copyright © 2019 YQ. All rights reserved.
//

import UIKit
import SnapKit

public class JMFactory: NSObject {

    public  class func creatLabel(title:String?,titleColor:UIColor,fontSize:CGFloat) -> JMLabel {

        let label:JMLabel = JMLabel.init()
        label.text = title
        label.textColor = titleColor
        label.font = UIFont.systemFont(ofSize: fontSize)

        return label

    }

    public  class func creatLabel(title:String?,titleColor:UIColor,fontSize:CGFloat,aligment:NSTextAlignment) -> JMLabel {

        let label:JMLabel = JMLabel.init()
        label.text = title
        label.textColor = titleColor
        label.font = UIFont.systemFont(ofSize: fontSize)
        label.textAlignment = aligment

        return label

    }


    public  class func creatLabel(frame:CGRect,title:String?,titleColor:UIColor,fontSize:CGFloat) -> JMLabel {

        let label:JMLabel = JMLabel.init()
        label.frame = frame;
        label.text = title
        label.textColor = titleColor
        label.font = UIFont.systemFont(ofSize: fontSize)

        return label

    }

    public class  func creatButton(title:String?,titleColor:UIColor?,fontSize:CGFloat,target:AnyObject?,selector:Selector?) -> JMButton {

        let button = JMButton.init(type: .custom)

        button.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        button.setTitleColor(titleColor, for: .normal)
        button.setTitle(title, for: .normal)
        
        if selector != nil && target != nil {
            button.addTarget(target!, action: selector!, for: .touchUpInside)
        }

        return button
    }

    public class  func creatButton(backGroundName:String,target:AnyObject?,selector:Selector?) -> JMButton {

        let button = JMButton.init(type: .custom)
        button.setBackgroundImage(UIImage.init(named: backGroundName), for: .normal)

        if selector != nil && target != nil {
            button.addTarget(target!, action: selector!, for: .touchUpInside)
        }

        return button
    }

    public class func creatButton(norImageName:String?,preImageName:String?,target:AnyObject?,selector:Selector?) -> JMButton{

        let button = JMButton.init(type: .custom)

        if norImageName != nil {
            button.setImage(UIImage(named: norImageName!), for: .normal)
        }

        if preImageName != nil {
            button.setImage(UIImage(named: preImageName!), for: .selected)
        }


        if selector != nil && target != nil {
            button.addTarget(target!, action: selector!, for: .touchUpInside)
        }


        return button

    }

    public class func creatButton(frame:CGRect,title:String,titleColor:UIColor,fontSize:CGFloat, norImageName:String?,preImageName:String?,target:AnyObject?,selector:Selector?) -> JMButton{

        let button = JMButton.init(type: .custom)

        button.frame = frame;
        button.setTitle(title, for: .normal)
        button.setTitleColor(titleColor, for: .normal)

        if norImageName != nil {
            button.setImage(UIImage(named: norImageName!), for: .normal)
        }

        if preImageName != nil {
            button.setImage(UIImage(named: preImageName!), for: .selected)
        }

        button.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)

        if selector != nil && target != nil {
            button.addTarget(target!, action: selector!, for: .touchUpInside)
        }

        return button

    }


    public class func creatField(frame:CGRect,textFontSize:CGFloat,placehoder:String,deleagte:UITextFieldDelegate?) -> JMTextField{

        let field = JMTextField.init()

        field.frame = frame;
        field.font = UIFont.systemFont(ofSize: textFontSize)
        field.placeholder = placehoder

        if deleagte != nil  {

            field.delegate = deleagte
        }

        return field

    }


    public class  func addTipLabel(tipTitle:String,tipColor:UIColor,tipFontSize:CGFloat,addLabel:JMLabel,superView:UIView,topMargin:CGFloat,Space:CGFloat)  {

        let tipLabel = JMFactory.creatLabel(title:tipTitle , titleColor: tipColor, fontSize: tipFontSize)

        superView.addSubview(tipLabel)
        superView.addSubview(addLabel)

        tipLabel.snp.makeConstraints { (make) in

            make.left.equalTo( Space)
            make.top.equalTo(topMargin)
        }

        addLabel.snp.makeConstraints { (make) in

            make.top.equalTo(topMargin)
            make.right.equalTo( -Space)
        }


    }

    public class  func addTipLabel(tipString:String,tipfont:CGFloat,tipColor:UIColor,labelTitle:String,labelFont:CGFloat,labelColor:UIColor,superView:UIView,topMargin:CGFloat,Space:CGFloat)  -> JMLabel {

        let tipLabel = JMFactory.creatLabel(title:tipString , titleColor: tipColor, fontSize: tipfont)

        let addLabel = JMFactory.creatLabel(title:labelTitle , titleColor: labelColor, fontSize: labelFont)

        superView.addSubview(tipLabel)
        superView.addSubview(addLabel)

        tipLabel.snp.makeConstraints { (make) in

            make.left.equalTo( Space)
            make.top.equalTo(topMargin)
        }

        addLabel.snp.makeConstraints { (make) in

            make.top.equalTo(topMargin)
            make.right.equalTo(-Space)
        }

        return addLabel


    }



}

