//
//  JMNumberField.swift
//  JMKit
//
//  Created by JM on 2020/1/14.
//  Copyright © 2020 JM. All rights reserved.
//

import UIKit

public class JMNumberField: JMTextField,UITextFieldDelegate {

    public enum NumberSupportStyle:String {

        case SupportNumber       //只支持正数（无法输入.和其他符号）
        case SupportFloat       //支持浮点数

    }

    ///支持的内容类型：正数/浮点数
    private var supportStyle:NumberSupportStyle = .SupportNumber
    
    ///设置小数点后的位数
    public var limitAfterPointCount = 9999

    public convenience  init(frame: CGRect,textFontSize:CGFloat,placehoder:String,supportStyle:NumberSupportStyle) {

        self.init(frame: frame)
        self.font = UIFont.systemFont(ofSize: textFontSize)
        self.placeholder = placehoder
        self.supportStyle = supportStyle

        if self.supportStyle == .SupportNumber{

            self.keyboardType = .numberPad

        }else if  self.supportStyle == .SupportFloat{

            self.keyboardType = .decimalPad

        }


    }


    override init(frame: CGRect) {
        super.init(frame: frame)

        self.delegate = self


    }

    //swift中规定:重写控件的init(frame方法)或者init()方法.必须重写 init?(coder aDecoder: NSCoder)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")

    }

    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        print(string)
        print(range)


        if supportStyle == .SupportNumber {

            return self.isPurnInt(str: string)

        }

        if supportStyle == .SupportFloat {

            return self.isFloat(str: string)

        }

        return true


    }

    //是不是纯数字
    func isPurnInt(str:String) -> Bool {

        if str == "" {

            return true
        }

        if str.count == 0 {

            return false
        }

        let scan: Scanner = Scanner(string: str)

        var val:Int = 0

        return scan.scanInt(&val) && scan.isAtEnd

    }

    func isFloat(str:String) -> Bool {

        if str == "" {

            return true
        }

        if self.text!.count  == 0 && str == "."{
            return false
        }

        if !self.text!.contains(".") && str == "." {

            return true
        }


        let pattern = "^[0-9]+([.]{1}[0-9]+){0,1}$"

        guard NSPredicate(format: "SELF MATCHES %@", pattern).evaluate(with: str) else{

            return false

        }

        return true
    }

    override func limitString() {


        if self.text == "" {

            return;
        }

        if self.getNumberAfterPoint() > Int(limitAfterPointCount) {

            self.text = self.text!.substring(to: self.text!.count - self.getNumberAfterPoint() + Int(limitAfterPointCount))

        }


    }

    //获取小数点后面的位数
    func getNumberAfterPoint() ->(Int) {

        if self.text!.contains(".") {

            let array = self.text!.components(separatedBy:".")
            if array.count == 2 {

                return array[1].count
            }

            return 0

        }

        return 0
    }






}
