//
//  JMLabel.swift
//  JMKit
//
//  Created by JM on 2019/8/30.
//  Copyright © 2019 YQ. All rights reserved.
//

import UIKit

public class JMLabel: UILabel {
    
    /// 自定义文本间距
    public var customSize: CGSize?

    public enum VerticalAlignment : Int {
        
        case   VerticalAlignmentMiddle = 0//设置文本居中
        case   VerticalAlignmentTop = 1   //设置文本从顶部开始
        case   VerticalAlignmentBottom = 2//设置文本紧贴底部
        
    }

    public var verticalAlignment = VerticalAlignment(rawValue: 0){

        didSet{

            self .setNeedsDisplay()

        }

    }

    public override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        
        var textRect:CGRect = super.textRect(forBounds: bounds, limitedToNumberOfLines: numberOfLines)
                
        switch verticalAlignment {
        
        case .VerticalAlignmentTop?:
            textRect.origin.y = bounds.origin.y
            
        case .VerticalAlignmentBottom?:
            textRect.origin.y = bounds.origin.y + bounds.size.height - textRect.size.height;

        default:
            textRect.origin.y = bounds.origin.y + (bounds.size.height - textRect.size.height) / 2.0;

        }

        return textRect

    }


    public override func drawText(in rect: CGRect) {

        let actualRect = self.textRect(forBounds: rect, limitedToNumberOfLines: self.numberOfLines)

        super.drawText(in: actualRect)

    }

}
