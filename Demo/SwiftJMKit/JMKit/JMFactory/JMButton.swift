//
//  JMButton.swift
//  JMKit
//
//  Created by JM on 2019/8/28.
//  Copyright © 2019 YQ. All rights reserved.
//

import UIKit


public class JMButton: UIButton {
    
    public enum ImageTitleListMode : Int {
        
        case   ImageLeftMode = 0   //文本在右，图片在左
        case   ImageRightMode = 1//文本在左，图片在右
        case   ImageTopMode = 2//文本在下，图片在上
        case   ImageBottomMode = 3//文本在上，图片在下
        
    }
    
    ///图片和文本排列的方式
    private var imageTitleMode = ImageTitleListMode(rawValue: 0)
    /// 图片和文本的间距
    private var imageTitleMargin:CGFloat = 0
    
    /// 设置的圆角值
    private var cornerValue:CGFloat = 0
    
    /// 设置的圆角类型
    private var cornerType = UIRectCorner()

    
    ///文本尺寸，默认是CGRect.zero
    public var titleRect = CGRect.zero
    ///图片尺寸，默认是CGRect.zero
    public var imageRect = CGRect.zero
    
    ///按钮的触摸范围整体变化，不能超出父视图
    public var enableTouchRect = CGRect.zero
    
    ///添加按钮的触摸范围，触摸范围增加（宽度*宽度），但是触摸范围不能超出父视图
    public var addTouchWidth:CGFloat = 0.0
    
    /// 自定义图片和文本尺寸,开启后可自由设置图片和文本的尺寸位置
    public var CustomImageTitleRect = false

    public override func titleRect(forContentRect contentRect: CGRect) -> CGRect {

        if(!self.titleRect.isEmpty && !self.titleRect.equalTo(CGRect.zero)){

            return self.titleRect
        }

        return super.titleRect(forContentRect: contentRect)

    }

    public override func imageRect(forContentRect contentRect: CGRect) -> CGRect {
        
        if(!self.imageRect.isEmpty && !self.imageRect.equalTo(CGRect.zero)){

            return self.imageRect
        }

        return super.imageRect(forContentRect: contentRect)

    }

    
    /// 设置按钮处于请求状态，无法点击
    public  func changeStatusOnRequest()  {

        self.alpha = 0.4
        self.isUserInteractionEnabled = false
    }
    
    /// 恢复按钮为正常状态
    public  func resumeNormalStatus()   {

        self.alpha = 1
        self.isUserInteractionEnabled = true

    }
    
    /// 设置按钮的图片和文本排列方式
    /// - Parameters:
    ///   - mode: 图片文本排列方式
    ///   - margin: 图片和文本间距
    public func changeImageTitleMode(mode:ImageTitleListMode,margin:CGFloat)  {

        self.imageTitleMode = mode
        self.imageTitleMargin = margin

    }
    
    /// 设置自定义圆角类型和圆角大小
    /// - Parameters:
    ///   - cornerValue: 圆角大小
    ///   - cornerType: 圆角类型
    public func setCustomCorner(cornerValue:CGFloat,cornerType:UIRectCorner)  {
        
        self.cornerValue = cornerValue
        self.cornerType = cornerType
        

    }


    public override func draw(_ rect: CGRect) {
        
        /*
         直接调用setNeedsDisplay或者setNeedsDisplayInRect:会触发drawRect：，但是有一个前提就是frame的size不能为0
         2、drawRect的调用时机是在viewWillAppear和viewDidAppear之间
         3、调用sizeToFit，会触发drawRect的调用
         4、UIView的contentMode属性设置成UIViewContentModeRedraw，每一次设置或更改frame值为触发drawRect的调用
         */
        
        if self.cornerValue > 0 {
            
            
            let boundRect = CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height)
            let maskPath = UIBezierPath(roundedRect: boundRect, byRoundingCorners: self.cornerType, cornerRadii: CGSize(width: self.cornerValue, height: self.cornerValue))
            let maskLayer = CAShapeLayer.init()
            maskLayer.frame = boundRect
            maskLayer.path = maskPath.cgPath
            self.layer.mask = maskLayer
            
        }
        
        
    }

    public override func layoutSubviews() {
        
        
        /*
         layoutSubViews的调用时机
         1、setNeedsLayout或者layoutIfNeed
         2、addSubview
         3、改变一个view的frame值的时候，当然前提是frame值前后发生了变化
         4、屏幕旋转的时候会触发父视图的layoutSubviews
         5、scrollView在滑动的过程中触发UIView重新布局的时候会执行layoutSubviews
         */


        super.layoutSubviews()

        let buttonSize:CGSize = self.bounds.size
        let imageRect:CGRect = self.imageRect(forContentRect: self.bounds)
        let titleRect:CGRect = self.titleRect(forContentRect: self.bounds)


        let margin:CGFloat = self.imageTitleMargin

        if addTouchWidth > 0 && self.enableTouchRect.equalTo(CGRect.zero) {
            
            self.enableTouchRect = CGRect(x: -addTouchWidth, y: -addTouchWidth, width: buttonSize.width+addTouchWidth*2, height: buttonSize.height + addTouchWidth*2)
        }

        if CustomImageTitleRect == true {

            self.imageView?.frame = imageRect
            self.titleLabel?.frame = titleRect

            return
        }


        //图片在上，文本在下
        if (self.imageTitleMode == ImageTitleListMode.ImageTopMode) {
            
            let totalHeight:CGFloat = imageRect.size.height + titleRect.size.height + margin;
            let y:CGFloat = buttonSize.height/2 - totalHeight/2;
            self.titleLabel?.textAlignment = NSTextAlignment.center

            self.imageView?.frame = CGRect(origin: CGPoint(x:buttonSize.width/2 - imageRect.size.width/2, y: y), size: imageRect.size)
            self.titleLabel?.frame = CGRect(origin: CGPoint(x:0, y: (self.imageView?.frame.maxY)! + margin), size: CGSize(width: buttonSize.width, height: titleRect.height))

        }

        if (self.imageTitleMode == ImageTitleListMode.ImageBottomMode) {
            
            
            let totalHeight:CGFloat = imageRect.size.height + titleRect.size.height + margin;
            let y:CGFloat = buttonSize.height/2 - totalHeight/2;
            self.titleLabel?.textAlignment = NSTextAlignment.center


            self.titleLabel?.frame = CGRect(origin: CGPoint(x:buttonSize.width/2 - titleRect.size.width/2, y:y), size: titleRect.size)

            self.imageView?.frame = CGRect(origin: CGPoint(x:buttonSize.width/2 - imageRect.size.width/2, y: (self.titleLabel?.frame.maxY)!+margin), size: imageRect.size)
        }




        if (self.imageTitleMode == ImageTitleListMode.ImageLeftMode) {

            let totalWidth:CGFloat = imageRect.size.width + titleRect.size.width + margin
            let x:CGFloat = buttonSize.width/2 - totalWidth/2

            self.titleLabel?.textAlignment = NSTextAlignment.left
            self.imageView?.frame = CGRect(origin: CGPoint(x:x, y:buttonSize.height/2 - imageRect.size.height/2), size: imageRect.size)

            self.titleLabel?.frame = CGRect(origin: CGPoint(x:(self.imageView?.frame.maxX)!+margin, y:buttonSize.height/2-titleRect.size.height/2), size: titleRect.size)

        }

        if (self.imageTitleMode == ImageTitleListMode.ImageRightMode) {

            let totalWidth:CGFloat = imageRect.size.width + titleRect.size.width + margin
            let x:CGFloat = buttonSize.width/2 - totalWidth/2

            self.titleLabel?.textAlignment = NSTextAlignment.right

            self.titleLabel?.frame = CGRect(origin: CGPoint(x:x, y:buttonSize.height/2 - titleRect.size.height/2), size: titleRect.size)
            self.imageView?.frame = CGRect(origin: CGPoint(x:(self.titleLabel?.frame.maxX)!+margin, y:buttonSize.height/2-imageRect.size.height/2), size: imageRect.size)

        }


    }

    public override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        

        if (self.enableTouchRect.equalTo(CGRect.zero) || self.enableTouchRect.isEmpty) {

            return super.point(inside: point, with: event);
        }

        if (self.enableTouchRect.contains(point)) {
            return true
        }

        return false

    }



}


