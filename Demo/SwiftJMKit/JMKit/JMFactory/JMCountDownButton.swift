//
//  JMCountDownButton.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/24.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

public class JMCountDownButton: UIButton {
    
    var timer:Timer?
    var time:NSInteger = 0
    var countTime:NSInteger = 0
    var normalText = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// 初始化倒计时按钮（倒计时内按钮不可点击）
    /// - Parameters:
    ///   - countTime: 倒计时时长
    ///   - normalTitle: 非倒计时时显示的文本
    public convenience init(countTime:NSInteger,normalTitle:String){
        
        self.init()
        
        self.setTitle(normalText, for: .normal)
        self.setTitleColor(.black, for: .normal)
        self.addTarget(self, action: #selector(beginTimer), for: .touchUpInside)
        
        self.time = countTime
        self.countTime = countTime
        self.normalText = normalTitle

        
    }
    
    
    
    func setupTimer()  {
        
        
        self.invalidateTimer()
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        
        RunLoop.main.add(timer!, forMode: .common)
        
    }
    
    @objc func beginTimer()  {
        
        self.time = self.countTime
        self.setTitle(String(format: "%lds", time), for: .normal)
        self.setupTimer()
        self.isUserInteractionEnabled = false

    }
    
    func endTimer()  {
        
        self.isUserInteractionEnabled = true
        self.invalidateTimer()
        self.setTitle(self.normalText, for: .normal)
        
    }
    
    @objc func timerAction()  {
        
        time -= 1
        self.setTitle(String(format: "%lds", time), for: .normal)
        if time == 0 {
            
            self.endTimer()
        }
        
        
        
    }
    
    func invalidateTimer()  {
        
        if timer == nil {
            
            return
        }
        
        timer?.invalidate()
        timer = nil
        
    }
    
    public override func willMove(toSuperview newSuperview: UIView?) {
            
        if newSuperview != nil {
            
            self.invalidateTimer()
        }
    }
    
    





    
}
