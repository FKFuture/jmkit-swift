//
//  JMCustomAlertView.swift
//  JMKit
//
//  Created by JM on 2020/1/11.
//  Copyright © 2020 JM. All rights reserved.
//

import UIKit

public class JMCustomAlertView: UIView {
    
    public typealias actionBlock = () -> Void

    
    lazy var titleLabel: JMLabel = {
        let label = JMLabel.init(frame: .zero)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        return label
    }()

    
    var suTitleLabel:JMLabel{
        
        let label = JMLabel.init(frame: .zero)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }
    
    var containBgView:JMButton?
    var buttonArray = NSMutableArray.init(capacity: 0)
    var blockArray = NSMutableArray.init(capacity: 0)


    override init(frame: CGRect) {
        super.init(frame: frame)

        self.backgroundColor = UIColor.init(white: 0, alpha: 0.4)
        self.frame = UIScreen.main.bounds

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public convenience init(title:String,subTitle:String){

        self.init(frame:CGRect.zero)
        self.titleLabel.text = title
        self.suTitleLabel.text = subTitle
    }
    
    /// 修改标题的字体颜色和大小
    /// - Parameters:
    ///   - font: 字体大小
    ///   - color: 字体颜色
    public func changeTitle(font:UIFont,color:UIColor)  {
        
        self.titleLabel.font = font
        self.titleLabel.textColor = color
    }
    
    public func changeSubTitle(font:UIFont,color:UIColor)  {
        
        self.suTitleLabel.font = font
        self.suTitleLabel.textColor = color
    }
    
    func addCenterListAction(actionList:NSArray,corner:CGFloat) {
        
        self.containBgView = JMButton()
        self.containBgView?.backgroundColor = UIColor.white
        self.containBgView?.setCornerRadius(radius: corner)
        
        let topLine = UIView()
        topLine.backgroundColor = UIColor.colorWithHexString(color: "#DFDFDF")
        self.containBgView?.addSubview(topLine)
        
        topLine.snp.makeConstraints { make in
            
            make.left.right.equalTo(0)
            make.height.equalTo(1)
            make.bottom.equalTo(-44)
            
        }
        
        self.addSubview(containBgView!)
        containBgView?.addSubview(titleLabel)
        containBgView?.addSubview(suTitleLabel)
        
        containBgView!.snp.makeConstraints { make in
            
            make.left.equalTo(52.5)
            make.right.equalTo(-52.5)
            make.center.equalTo(0)
            
        }
        
        titleLabel.snp.makeConstraints { make in
            
            make.top.equalTo(36)
            make.bottom.equalTo(-80)
            make.left.equalTo(10)
            make.right.equalTo(-10)

        }
        
        if suTitleLabel.text?.count ?? 0 > 0 {
            
            titleLabel.snp.remakeConstraints { make in
                
                make.top.equalTo(36)
                make.left.equalTo(10)
                make.right.equalTo(-10)

            }
            
            suTitleLabel.snp.remakeConstraints { make in
                
                make.top.equalTo(titleLabel.snp_bottom).offset(20)
                make.bottom.equalTo(-67.5)
                make.left.equalTo(20)
                make.right.equalTo(-20)

            }
            
        }
        
        guard actionList.count > 0 else {
            return
        }
        
        let rate = 1.0/Double(actionList.count)
        
        for i in 0..<actionList.count {
            
            let action:JMAlertAction = actionList[i] as! JMAlertAction
            let button = JMFactory.creatButton(title: action.title, titleColor: action.titleColor, fontSize: action.fontSize!, target: self, selector: #selector(actionClick(button:)))
            button.tag = i
            self.blockArray.add(action.callblock!)
            self.buttonArray.add(button)
            self.containBgView?.addSubview(button)
            
            if i<actionList.count - 1 {
                
                let rightLine = UIView()
                rightLine.backgroundColor = UIColor.colorWithHexString(color: "#DFDFDF")
                button .addSubview(rightLine)
                
                rightLine.snp.makeConstraints { make in
                    
                    make.top.bottom.right.equalTo(0)
                    make.width.equalTo(1)

                }

            }
            
            if i == 0 {
                
                button.snp.makeConstraints { make in
                    
                    make.width.equalTo(self.containBgView!.snp_width).multipliedBy(rate)
                    make.height.equalTo(44)
                    make.bottom.equalTo(0)
                    make.left.equalTo(0)

                }
                
            }else{
                
                let lastButton:JMButton = self.buttonArray[i-1] as! JMButton
                
                button.snp.makeConstraints { make in
                    
                    make.width.equalTo(self.containBgView!.snp_width).multipliedBy(rate)
                    make.height.equalTo(44)
                    make.bottom.equalTo(0)
                    make.left.equalTo(lastButton.snp_right).offset(0)

                }
                
            }
            
            
            
        }
        
        
    }
    
    func addBottomListAction(actionList:NSArray) {
        
        self.containBgView = JMButton()
        self.containBgView?.backgroundColor = UIColor.white
        self.addSubview(containBgView!)

                
        containBgView?.addSubview(titleLabel)
        containBgView?.addSubview(suTitleLabel)
        
        containBgView!.snp.makeConstraints { make in
            
            make.left.equalTo(10)
            make.right.equalTo(-10)
            
        }
        
        self.initBottomTitleViewMasonry()
        
        guard actionList.count > 0 else {
            return
        }
        
        var bottomMargin:CGFloat = 50
                
        for i in 0..<actionList.count {
            
            let action:JMAlertAction = actionList[i] as! JMAlertAction
            let button = JMFactory.creatButton(title: action.title, titleColor: action.titleColor, fontSize: action.fontSize!, target: self, selector: #selector(actionClick(button:)))
            button.tag = i
            self.blockArray.add(action.callblock!)
            self.buttonArray.add(button)
            self.addSubview(button)
            
            if i == actionList.count - 1 {
                
                containBgView!.snp.makeConstraints { make in
                    
                    make.bottom.equalTo(button.snp_top).offset(-action.topMargin!);

                }
                
                if action.topMargin! > 0 {
                    
                    self.containBgView?.setCornerRadius(radius:action.cornerValue)
                    
                }else{
                    
                    let cornerType:UIRectCorner = [UIRectCorner.topLeft, UIRectCorner.topRight]
                    
                    self.containBgView?.setCustomCorner(cornerValue: action.cornerValue!, cornerType: cornerType)
                }

            }
            
            if i == 0 {
                
                if action.topMargin! > 0 {
                    
                    self.containBgView?.setCornerRadius(radius: action.cornerValue)
                    
                }else{
                    
                    let cornerType:UIRectCorner = [UIRectCorner.bottomLeft, UIRectCorner.bottomRight]
                    
                    self.containBgView?.setCustomCorner(cornerValue: action.cornerValue!, cornerType: cornerType)
                }

            }else{
                
                let lastAction:JMAlertAction = actionList[i-1] as! JMAlertAction
                
                if lastAction.topMargin > 0 && action.topMargin > 0 {
                    
                    button.setCornerRadius(radius: action.cornerValue)
                    
                }else if (lastAction.topMargin > 0 && action.topMargin <= 0 ){
                    
                    button.setCustomCorner(cornerValue: action.cornerValue, cornerType: [UIRectCorner.bottomLeft,UIRectCorner.bottomRight])
                    
                }else if (lastAction.topMargin <= 0 && action.topMargin > 0 ){
                    
                    button.setCustomCorner(cornerValue: action.cornerValue, cornerType: [UIRectCorner.topLeft,UIRectCorner.topRight])

                    
                }
                
                if lastAction.topMargin <= 0 {
                    
                    let bottomLine = UIView()
                    bottomLine.backgroundColor = UIColor.colorWithHexString(color: "#DFDFDF")
                    button.addSubview(bottomLine)
                    
                    bottomLine.snp.makeConstraints { make in
                        
                        make.left.bottom.right.equalTo(0)
                        make.height.equalTo(1)
                    }
                    
                }
                
                if action.topMargin <= 0 && i == actionList.count - 1 {
                    
                    let bottomLine = UIView()
                    bottomLine.backgroundColor = UIColor.colorWithHexString(color: "#DFDFDF")
                    button.addSubview(bottomLine)
                    
                    bottomLine.snp.makeConstraints { make in
                        
                        make.left.top.right.equalTo(0)
                        make.height.equalTo(1)
                    }
                    
                    
                }
                
                

                
                
            }
            
            button.snp.makeConstraints { make in
                
                make.left.equalTo(action.paddMargin)
                make.right.equalTo(-action.paddMargin)
                make.bottom.equalTo(-bottomMargin)
                make.height.equalTo(action.height)
            }
            
            
            bottomMargin += action.height + action.topMargin
            
            
            
        }
        
        
    }
    
    func initBottomTitleViewMasonry()  {
        
    }

    
    @objc func actionClick(button:JMButton)  {
        
        let block:actionBlock = self.blockArray[button.tag] as! JMCustomAlertView.actionBlock
        
        block()
        
        self.close()
        
    }



    public func show()  {


        UIApplication.shared.keyWindow?.addSubview(self)

    }

    public func close()  {

        self.removeFromSuperview()


    }
    
}

public class JMAlertAction: NSObject {
    
    public typealias actionBlock = () -> Void
    public var title:String?
    public var titleColor:UIColor?
    public var fontSize:CGFloat!
    public var height:CGFloat!
    public var paddMargin:CGFloat!
    public var topMargin:CGFloat!
    public var cornerValue:CGFloat!
    var callblock:actionBlock!
    
    public override init() {
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public convenience init(title:String,topMargin:CGFloat,block:@escaping actionBlock){

        self.init()
        self.initData()
        self.title = title
        self.topMargin = topMargin
        self.callblock = block
    }
    
    func initData()  {
        
        self.height = 57;
        self.paddMargin = 10;
        self.topMargin = 10;
        self.cornerValue = 13;
        self.fontSize = 16;

    }


}



