//
//  JMDefaultAlertView.swift
//  SwiftJMKit
//
//  Created by JM on 2022/2/24.
//  Copyright © 2022 JM. All rights reserved.
//

import UIKit

class JMDefaultAlertView: UIView {

    var clickBgDismiss = false
    var contentView:UIView?


    override init(frame: CGRect) {
        super.init(frame: frame)

        self.backgroundColor = UIColor.init(white: 0, alpha: 0.4)
        self.frame = UIScreen.main.bounds

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public convenience init(customView:UIView){

        self.init(frame:CGRect.zero)
        self.addSubview(customView)
        contentView = customView


    }


    public func show()  {


        UIApplication.shared.keyWindow?.addSubview(self)

    }

    public func close()  {

        self.removeFromSuperview()


    }

    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {


        guard contentView != nil else{

            return
        }

        guard clickBgDismiss != false else{

            return
        }


        let touch:UITouch = (touches as NSSet).anyObject() as! UITouch     //进行类  型转化

        let touchView:UIView? = touch.view


        if touchView != contentView {

            self.removeFromSuperview()

        }

        
    }

}
